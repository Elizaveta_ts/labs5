#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>



void clearCanvas(char **buf, int height, int width) //������� ������� ������� 
{	int i, j;
	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
			buf[i][j] = ' ';
}

void fillCanvas(char **buf, int height, int width) //������� ���������� �������
{
	int i, j;
	for (i = 0; i < height / 2; i++) //����� ������� �������
		for (j = 0; j < width / 2; j++)
			buf[i][j] = (rand()%2 == 1)?'*':' ';

	for (i = height / 2; i < height; i++)  //����� ������ �������
		for (j = 0; j < width / 2; j++)
			buf[i][j] = buf[i - height / 2][j];

	for (i = height / 2; i < height; i++) //������ ������ �������
		for (j = width / 2; j < width; j++)
			buf[i][j] = buf[i - height / 2][j - width / 2];

	for (i = 0; i < height / 2; i++) //������ ������� �������
		for (j = width / 2; j < width; j++)
			buf[i][j] = buf[i][j - width / 2];

}

void PrintCanvas(char **buf, int height, int width)//������ �������
{
	int i, j;
	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++)
			putchar(buf[i][j]);
		putchar('\n');
	}
}

int main()
{
	int HEIGHT = 0, WIDTH = 0, i = 0;
	printf("Please enter height of massive\n"); //����� ������������ ��� ������ ������ � ������ �������
	scanf("%d", &HEIGHT);
	printf("Please enter width of massive\n");
	scanf("%d", &WIDTH);
	char ** buf = (char **)calloc(HEIGHT, sizeof(char*));

	clock_t now;
	srand(time(NULL));

	for (i = 0; i < HEIGHT; i++)
		buf[i] = (char *)calloc(WIDTH, sizeof(char));

	while (1)
	{
		clearCanvas(buf,HEIGHT, WIDTH);//������� ������
		fillCanvas(buf, HEIGHT, WIDTH); //���������� �������
		system("cls"); //������� �����
		PrintCanvas(buf, HEIGHT, WIDTH); //�������� ������
		now = clock();
		while (clock()<now + CLOCKS_PER_SEC); //��������� ��������
	}

	for (i = 0; i < HEIGHT; i++)
		free(buf[i]);

	free(buf);

	return 0;

}